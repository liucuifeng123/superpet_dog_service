package com.superpet.common.model;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

/**
 * Generated by JFinal, do not modify this file.
 * <pre>
 * Example:
 * public void configPlugin(Plugins me) {
 *     ActiveRecordPlugin arp = new ActiveRecordPlugin(...);
 *     _MappingKit.mapping(arp);
 *     me.add(arp);
 * }
 * </pre>
 */
public class _MappingKit {
	
	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("p_key_value", "id", KeyValue.class);
		arp.addMapping("p_pet_card", "id", PetCard.class);
		arp.addMapping("p_pet_card_bg", "id", PetCardBg.class);
		arp.addMapping("p_pet_card_words", "id", PetCardWords.class);
		arp.addMapping("p_pet_feed_log", "id", PetFeedLog.class);
		arp.addMapping("p_pet_feed_total", "id", PetFeedTotal.class);
		arp.addMapping("p_pet_house", "id", PetHouse.class);
		arp.addMapping("p_pet_pic", "id", PetPic.class);
		arp.addMapping("p_pets", "id", Pets.class);
		arp.addMapping("p_users", "id", Users.class);
		arp.addMapping("p_wx_run", "id", WxRun.class);
		arp.addMapping("p_wx_token", "id", WxToken.class);
	}
}

