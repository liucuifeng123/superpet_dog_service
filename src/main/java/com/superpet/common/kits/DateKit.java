package com.superpet.common.kits;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateKit {
	
	private static String dateFormat = "yyyy-MM-dd";
	private static String timeFormat = "yyyy-MM-dd HH:mm:ss";

	//时间对象转时间字符串
	public static String toTimeStr(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat(timeFormat);
		return formatter.format(date);
	}
	//日期对象转日期字符串
	public static String toDateStr(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		return formatter.format(date);
	}
	//日期字符串转日期对象
	public static Date strToDate(String dateStr) throws ParseException{
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		return formatter.parse(dateStr);
	}

	/**
	 * 获取n偏移量后的日期，例如：date=new Date(),n=7，则获取7天后的日期，若n=-7，则获取7天前的日期
	 * @param date
	 * @param n
	 * @return
	 */
	public static String strToDateOffset(Date date, Integer n){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, n);
		Date result = calendar.getTime();
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		return formatter.format(result);
	}

	/**
	 * 判断一个时间戳（数字格式的时间）是不是昨天
	 * @param args
	 */
	public static boolean isYesterday(Long timestamp){
		Date date = new Date();
		date.setTime(timestamp);
		String yesterday = DateKit.strToDateOffset(new Date(), -1);
		if(yesterday.equals(DateKit.toDateStr(date))){
			return true;
		}else{
			return false;
		}
	}

	public static final String[] zodiacArray = { "猴", "鸡", "狗", "猪", "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊" };

	public static final String[] constellationArray = { "水瓶座", "双鱼座", "牡羊座",
			"金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "魔羯座" };

	public static final int[] constellationEdgeDay = { 20, 19, 21, 21, 21, 22, 23, 23, 23, 23, 22, 22 };

	public static String date2Zodica(Calendar time) {
		return zodiacArray[time.get(Calendar.YEAR) % 12];
	}

	public static String date2Zodica(String time) {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		try {
			date = sdf.parse(time);
			c.setTime(date);

			String zodica = date2Zodica(c);
			System.out.println("生肖：" + zodica);
			return zodica;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 根据日期获取星座
	 *
	 * @param time
	 * @return
	 */
	public static String date2Constellation(String time) {

		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		try {
			date = sdf.parse(time);
			c.setTime(date);

			String constellation = date2Constellation(c);
			System.out.println("星座：" + constellation);
			return constellation;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * 根据日期获取星座
	 *
	 * @param time
	 * @return
	 */
	public static String date2Constellation(Calendar time) {
		int month = time.get(Calendar.MONTH);
		int day = time.get(Calendar.DAY_OF_MONTH);
		if (day < constellationEdgeDay[month]) {
			month = month - 1;
		}
		if (month >= 0) {
			return constellationArray[month];
		}
		// default to return 魔羯
		return constellationArray[11];
	}

}
