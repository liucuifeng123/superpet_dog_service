#define updateByIdList(table, col, val, idList)
  update #(table) set #(col) = #(val) where id in (
    #for(id : idList)
      #(for.index >0 ? ", " : "") #(id)
    #end
  )
#end

#sql("getUserPetList")
  select id,
  petNo,
  petName,
  ROUND(timestampdiff(day,birthday,now())/365,2) age,
  concat((select pvalue from p_key_value where pkey='IMG_PATH_PREFIX'),avatar) avatar,
  sex,
  isNeuter,
  weight,
  feature,
  userid,
  userNickName,
  totalEnergy,
  constellation,
  updateTime,
  breed
  from p_pets
  where userid = #para(0)
  and activeFlag=1
  order by updateTime desc
#end

#sql("getPetById")
  select id,
  petNo,
  petName,
  birthday,
  ROUND(timestampdiff(day,birthday,now())/365,2) age,
  concat((select pvalue from p_key_value where pkey='IMG_PATH_PREFIX'),avatar) avatar,
  sex,
  isNeuter,
  weight,
  feature,
  userid,
  userNickName,
  totalEnergy,
  constellation,
  updateTime,
  breed,
  levelNo,
  picLikeCount,
  nextLevelNeed
  from p_pets
  where id = #para(0)
#end

#sql("getUserPetPicList")
  select id,
  petid,
  concat((select pvalue from p_key_value where pkey='IMG_PATH_PREFIX'),picPath) picPath,
  updateTime
  from p_pet_pic
  where petid = #para(0)
  and activeFlag=1
  order by updateTime desc
  limit #para(1)
#end

#sql("getUserPetFeedTotalList")
  select u.avatar,u.nickName,fe.feedVal
  from p_pet_feed_total fe
  join p_users u on u.id = fe.userid
  where fe.petid = #para(0)
  and fe.feedVal>0
  order by fe.feedVal desc
  limit 20
#end

#sql("getUserPetFeedByDayList")
  select u.id userId, u.avatar,u.nickName,fe.feedVal,u.petsCount
  from p_pet_feed_log fe
  join p_users u on u.id = fe.userid
  where fe.petid = #para(0)
  and fe.feedDate = #para(1)
  order by fe.feedVal desc
  limit 20
#end

#sql("getTodayFeedPet")
  select * from p_pet_feed_log
  where petid = #para(0)
  and userid = #para(1)
  and feedDate = #para(2)
  limit 1
#end

#sql("getPetFeedTotal")
  select * from p_pet_feed_total
  where userid = #para(0)
  and petid = #para(1)
  limit 1
#end

#sql("getPetHouse")
  select * from p_pet_house
  where userid = #para(0)
  and petid = #para(1)
  limit 1
#end

#sql("getPetHouseList")
  select concat((select pvalue from p_key_value where pkey='IMG_PATH_PREFIX'),p.avatar) avatar
  ,p.petName,p.totalEnergy,u.nickName,u.avatar uAvatar,p.id petId,u.id userId
  ,p.petNo,ROUND(timestampdiff(day,p.birthday,now())/365,2) age,p.birthday
  ,p.sex,p.isNeuter,p.weight,p.feature,p.constellation,p.breed, p.levelNo
  from p_pet_house ph
  join p_pets p on p.id = ph.petid
  join p_users u on u.id = ph.petUserid
  where ph.userid = #para(0)
  and ph.activeFlag=1
  order by p.totalEnergy desc
  limit 20
#end

#sql("getMyFeedPetList")
  select concat((select pvalue from p_key_value where pkey='IMG_PATH_PREFIX'),p.avatar) avatar
  ,p.petName,u.nickName,u.avatar uAvatar,f.feedVal,f.feedDate,u.petsCount,u.id userId,p.id petId
  ,SUBSTR(f.updateTime,12,5) updateTime
  from p_pet_feed_log f
  join p_pets p on p.id = f.petid
  join p_users u on u.id = f.petUserid
  where f.userid = #para(0)
  order by f.updateTime desc
  limit 30
#end

#sql("getFeedMyPetList")
  select concat((select pvalue from p_key_value where pkey='IMG_PATH_PREFIX'),p.avatar) avatar
  ,p.petName,u.nickName,u.avatar uAvatar,f.feedVal,f.feedDate,u.petsCount,u.id userId,p.id petId
  ,SUBSTR(f.updateTime,12,5) updateTime
  from p_pet_feed_log f
  join p_pets p on p.id = f.petid
  join p_users u on u.id = f.userid
  where f.petUserid = #para(0)
  order by f.updateTime desc
  limit 30
#end

#sql("deletePetPic")
  #@updateByIdList("p_pet_pic","activeFlag","0", idList)
#end

#sql("getPetCardList")
  select id,
  userid,
  masterName,
  petName,
  concat((select pvalue from p_key_value where pkey='IMG_PATH_PREFIX'),cardPic) cardPic,
  cardContent,
  updateTime,
  from p_pet_card
  where userid = #para(0)
  and activeFlag = 1
  order by updateTime desc
#end

#sql("getPetCardBg")
  select concat((select pvalue from p_key_value where pkey='IMG_PATH_PREFIX'),cardBg) cardBg
  from p_pet_card_bg where activeFlag=1 limit 1
#end

#sql("getCardWords123")
  select * from p_pet_card_words where cardWordType in (1,2,3) and activeFlag=1 order by cardWordType
#end
